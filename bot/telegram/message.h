#pragma once

#include <string>
#include <jsoncpp/json/json.h>

class Message {
public:
    explicit Message(Json::Value value);

    const std::string& GetContent() const;

    const int64_t& GetChatId() const;

    const int64_t& GetUpdateId() const;

    const int64_t& GetMessageId() const;

private:
    std::string text_content_;
    int64_t chat_id_;
    int64_t update_id_;
    int64_t message_id_;
    Json::Value value_;
};

#include <random>
#include <fstream>

#include "bot.h"
#include "exceptions.h"
#include "logger.h"

#include <Poco/JSON/Object.h>
#include <Poco/JSON/Parser.h>
#include <Poco/Net/HTTPResponse.h>
#include <Poco/Net/HTTPRequest.h>
#include <Poco/Net/HTTPClientSession.h>
#include <Poco/StreamCopier.h>
#include <Poco/URI.h>

class Bot::BotImpl {
public:
    BotImpl(const std::string& api_url, const std::string& token, std::string offset_file);

    ~BotImpl();

    void ProcessRandom(int64_t chat_id, const std::shared_ptr<Client>& client);
    void ProcessWeatherRequest(int64_t chat_id, const std::shared_ptr<Client>& client);
    void ProcessStyleGuide(int64_t chat_id, const std::shared_ptr<Client>& client);
    void ProcessGif(int64_t chat_id, const std::shared_ptr<Client>& client);
    void ProcessSticker(int64_t chat_id, const std::shared_ptr<Client>& client);
    void ProcessUnknownCommand(int64_t chat_id, const std::shared_ptr<Client>& client);
    void ProcessCrash();
    void ProcessStop();

    void Process(const Message& message);

    void Loop();

    int64_t GetOffset() const;

    void SetOffset(int64_t offset);

    void SaveOffset(int64_t offset);

    int64_t ReadOffset();

    void SetGifCollection(std::vector<std::string> collection);

    void SetStickerCollection(std::vector<std::string> collection);

private:
    std::mt19937 generator_;
    std::uniform_int_distribution<> distribution_;
    std::shared_ptr<Client> client_;
    int64_t offset_;
    std::string offset_file_;
    std::vector<std::string> gifachi_collection_;
    std::vector<std::string> stickers_collection_;
};

Bot::BotImpl::BotImpl(const std::string& api_url, const std::string& token, std::string offset_file)
    : generator_(std::random_device()()), offset_file_(std::move(offset_file)) {
    client_ = std::make_shared<Client>(api_url, token);
    offset_ = ReadOffset();
    GetLogger().Info("Starting bot...");
}

Bot::BotImpl::~BotImpl() {
    SaveOffset(offset_);
    GetLogger().Info("Shutting Down");
}

void Bot::BotImpl::ProcessRandom(int64_t chat_id, const std::shared_ptr<Client>& client) {
    std::string random_number = std::to_string(distribution_(generator_));
    client->SendMessage(chat_id, random_number);
    GetLogger().Info("Sent " + random_number + " on /random request");
}

void Bot::BotImpl::ProcessWeatherRequest(int64_t chat_id, const std::shared_ptr<Client>& client) {
    client->SendMessage(chat_id, "Winter is coming!");
    GetLogger().Info("Sent \"Winter is coming!\" on /weather request");
}

void Bot::BotImpl::ProcessStyleGuide(int64_t chat_id, const std::shared_ptr<Client>& client) {
    client->SendPhoto(chat_id,
                      "https://cdn-images-1.medium.com/max/800/1*NSVfi_R4_UrJ12PRJ6Rg5Q.jpeg");
    GetLogger().Info("Sent \"Some joke!\" on /styleguide request");
}

void Bot::BotImpl::ProcessGif(int64_t chat_id, const std::shared_ptr<Client>& client) {
    if (gifachi_collection_.empty()) {
        client->SendMessage(chat_id,
                            "There is no gif, but i\'ll sing a song for you!\n"
                            "Bla-bla-bla-blaaaa! Ta-ta-ta-taa-taaaa! :))");
        return;
    }
    int64_t idx = distribution_(generator_) % gifachi_collection_.size();
    client->SendPhoto(chat_id, gifachi_collection_[idx]);
    GetLogger().Info("Sent " + gifachi_collection_[idx] + " on /gif request");
}

void Bot::BotImpl::ProcessSticker(int64_t chat_id, const std::shared_ptr<Client>& client) {
    if (stickers_collection_.empty()) {
        client->SendMessage(chat_id,
                            "There is no stickers, but i\'ll sing a song for you!\n"
                            "Bla-bla-bla-blaaaa! Ta-ta-ta-taa-taaaa! :))");
        return;
    }
    int64_t idx = distribution_(generator_) % stickers_collection_.size();
    client->SendSticker(chat_id, stickers_collection_[idx]);
    GetLogger().Info("Sent " + stickers_collection_[idx] + " on /sticker request");
}

void Bot::BotImpl::ProcessUnknownCommand(int64_t chat_id, const std::shared_ptr<Client>& client) {
    std::string screen =
        "I see that you got lost! Here are some tips that may help you:\n"
        "/random - to get a random number from bot\n"
        "/weather - to get the current (or not) weather \n"
        "/styleguide - to have fun about code review\n"
        "/stop - to stop bot\n"
        "/crash - to break it\n"
        "/gif - to get a funny picture\n"
        "/sticker - to get sticker\n";
    client->SendMessage(chat_id, screen);
}

void Bot::BotImpl::Loop() {
    while (true) {
        auto updates = client_->GetUpdates(offset_);
        for (const auto& upd : updates) {
            offset_ = std::max(upd.GetUpdateId() + 1, offset_);
            Process(upd);
        }
    }
}

void Bot::MainLoop() {
    try {
        while (true) {
            impl_->Loop();
        }
    } catch (TelegramApiError& ex) {
        GetLogger().Error(std::string("Got an error: ") + ex.what());
        throw;
    } catch (JsonConvertationError& ex) {
        GetLogger().Error(std::string("Got an error: ") + ex.what());
        throw;
    } catch (StopError& ex) {
        impl_->SaveOffset(impl_->GetOffset());
        GetLogger().Error("Got request to stop...");
        return;
    } catch (...) {
        impl_->SaveOffset(impl_->GetOffset());
        GetLogger().Error("CRASH! CRASH! CRASH! Aborting...");
        throw;
    }
}

void Bot::BotImpl::ProcessCrash() {
    GetLogger().Error("CRASH! CRASH! CRASH! Aborting...");
    abort();
}

void Bot::BotImpl::ProcessStop() {
    throw StopError("Caused stop bot");
}

void Bot::BotImpl::Process(const Message& message) {
    if (message.GetContent() == "/random") {
        ProcessRandom(message.GetChatId(), client_);
    } else if (message.GetContent() == "/styleguide") {
        ProcessStyleGuide(message.GetChatId(), client_);
    } else if (message.GetContent() == "/weather") {
        ProcessWeatherRequest(message.GetChatId(), client_);
    } else if (message.GetContent() == "/gif") {
        ProcessGif(message.GetChatId(), client_);
    } else if (message.GetContent() == "/sticker") {
        ProcessSticker(message.GetChatId(), client_);
    } else if (message.GetContent() == "/stop") {
        ProcessStop();
    } else if (message.GetContent() == "/crash") {
        ProcessCrash();
    } else {
        ProcessUnknownCommand(message.GetChatId(), client_);
        GetLogger().Info("Unhandled command: " + message.GetContent());
    }
}

int64_t Bot::BotImpl::GetOffset() const {
    return offset_;
}

void Bot::BotImpl::SetOffset(int64_t offset) {
    offset_ = offset;
}

void Bot::BotImpl::SaveOffset(int64_t offset) {
    std::ofstream fout(offset_file_);
    fout.clear();
    fout << offset;
    fout.close();
}

int64_t Bot::BotImpl::ReadOffset() {
    int64_t offset = 0;
    std::ifstream fin(offset_file_);
    if (!fin.eof()) {
        fin >> offset;
    }
    fin.close();
    return offset;
}

void Bot::BotImpl::SetGifCollection(std::vector<std::string> collection) {
    gifachi_collection_ = std::move(collection);
}

Bot::Bot(std::string base_api_uri, std::string token, std::string offset_file)
    : base_api_uri_(std::move(base_api_uri)),
      offset_file_(std::move(offset_file)),
      token_(std::move(token)) {
    impl_ = std::make_shared<BotImpl>(base_api_uri_, token_, offset_file_);
}

void Bot::SetGifCollection(std::vector<std::string> gifs) {
    impl_->SetGifCollection(std::move(gifs));
}

void Bot::SetStickerCollection(std::vector<std::string> gifs) {
    impl_->SetStickerCollection(std::move(gifs));
}

void Bot::BotImpl::SetStickerCollection(std::vector<std::string> collection) {
    stickers_collection_ = std::move(collection);
}

#include "logger.h"

#include <Poco/Logger.h>
#include <Poco/FileChannel.h>
#include <Poco/AutoPtr.h>

class Logger::LoggerImpl {
public:
    LoggerImpl(const std::string& log_storage);
    void Info(const std::string& msg);
    void Debug(const std::string& msg);
    void Error(const std::string& msg);
    void Warn(const std::string& msg);

private:
    Poco::Logger& logger_;
};

Logger::LoggerImpl::LoggerImpl(const std::string& log_storage)
    : logger_(Poco::Logger::get("Main Logger")) {
    auto channel = Poco::AutoPtr<Poco::FileChannel>(new Poco::FileChannel);
    channel->setProperty("path", log_storage);
    logger_.setChannel(channel);
}

void Logger::LoggerImpl::Info(const std::string& msg) {
    logger_.information(msg);
}
void Logger::LoggerImpl::Debug(const std::string& msg) {
    logger_.debug(msg);
}
void Logger::LoggerImpl::Error(const std::string& msg) {
    logger_.error(msg);
}
void Logger::LoggerImpl::Warn(const std::string& msg) {
    logger_.warning(msg);
}

Logger::Logger(std::string log_file) : log_file_(std::move(log_file)) {
    impl_ = std::make_shared<LoggerImpl>(log_file_);
}

void Logger::Info(const std::string& msg) {
    impl_->Info(msg);
}
void Logger::Debug(const std::string& msg) {
    impl_->Debug(msg);
}
void Logger::Error(const std::string& msg) {
    impl_->Error(msg);
}
void Logger::Warn(const std::string& msg) {
    impl_->Warn(msg);
}

Logger& GetLogger() {
    static Logger logger("log.txt");
    return logger;
}

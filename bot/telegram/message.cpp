#include "message.h"
#include <jsoncpp/json/json.h>

Message::Message(Json::Value value) {
    value_ = std::move(value);
    auto message = value_.get("message", Json::Value());
    text_content_ = message.get("text", Json::Value()).asString();
    message_id_ = message.get("message_id", Json::Value()).asInt64();
    chat_id_ = message.get("chat", Json::Value()).get("id", Json::Value()).asInt64();
    update_id_ = value_.get("update_id", Json::Value()).asInt64();
}

const std::string& Message::GetContent() const {
    return text_content_;
}

const int64_t& Message::GetChatId() const {
    return chat_id_;
}

const int64_t& Message::GetUpdateId() const {
    return update_id_;
}

const int64_t& Message::GetMessageId() const {
    return message_id_;
}

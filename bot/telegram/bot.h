#pragma once

#include <string>
#include <vector>
#include <iostream>
#include <jsoncpp/json/json.h>
#include "client.h"
#include "exceptions.h"
#include "logger.h"

class Bot {
public:
    Bot(std::string base_api_uri = "https://api.telegram.org",
        std::string token = "769340264:AAEAgT1tz6ks4aIaE4g_J_W46HJPmg5RtCA",
        std::string offset_file = "offset.tmp");

    void MainLoop();

    void SetGifCollection(std::vector<std::string> gifs);
    void SetStickerCollection(std::vector<std::string> gifs);

private:
    class BotImpl;

    std::shared_ptr<BotImpl> impl_;

    std::string base_api_uri_;
    std::string offset_file_;
    std::string token_;
};

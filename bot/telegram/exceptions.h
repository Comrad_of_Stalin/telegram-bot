#pragma once

#include <stdexcept>

class TelegramApiError : public std::runtime_error {
public:
    TelegramApiError(int http_code, const std::string& msg)
        : std::runtime_error("api error: response code: " + std::to_string(http_code) +
                             " details: " + msg),
          http_code_(http_code),
          msg_(msg) {
    }
    int http_code_;
    std::string msg_;
};

class JsonConvertationError : public std::runtime_error {
public:
    JsonConvertationError(const std::string& msg) : std::runtime_error(msg) {
    }
};

class StopError : public std::runtime_error {
public:
    StopError(const std::string& msg) : std::runtime_error(msg) {
    }
};

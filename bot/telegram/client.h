#pragma once

#include <memory>
#include <jsoncpp/json/json.h>

class Message {
public:
    explicit Message(Json::Value value) {
        value_ = std::move(value);
        text_content_ = value_.get("message", Json::Value()).get("text", Json::Value()).asString();
        message_id_ =
            value_.get("message", Json::Value()).get("message_id", Json::Value()).asInt64();
        chat_id_ = value_.get("message", Json::Value())
                       .get("chat", Json::Value())
                       .get("id", Json::Value())
                       .asInt64();
        update_id_ = value_.get("update_id", Json::Value()).asInt64();
    }

    const std::string& GetContent() const {
        return text_content_;
    }

    const int64_t& GetChatId() const {
        return chat_id_;
    }

    const int64_t& GetUpdateId() const {
        return update_id_;
    }

    const int64_t& GetMessageId() const {
        return message_id_;
    }

private:
    std::string text_content_;
    int64_t chat_id_;
    int64_t update_id_;
    int64_t message_id_;
    Json::Value value_;
};

class Client {
public:
    Client(const std::string& base_url, const std::string& token);

    void GetMe();

    void SendMessage(int64_t chat_id, const std::string& msg, int64_t reply_msg_id = 0);

    void SendPhoto(int64_t chat_id, const std::string& picture_path, int64_t reply_msg_id = 0,
                   const std::string& caption = "");

    void SendSticker(int64_t chat_id, const std::string& sticker_path);

    std::vector<Message> GetUpdates(int64_t offset = 0, int64_t timeout = 0);

private:
    class ClientImpl;
    std::shared_ptr<ClientImpl> impl_;
};

#include <unordered_map>
#include <vector>
#include <string>
#include <random>
#include <sstream>

#include "client.h"
#include "exceptions.h"

#include <Poco/Net/HTTPResponse.h>
#include <Poco/Net/HTTPRequest.h>
#include <Poco/Net/HTTPClientSession.h>
#include <Poco/Net/HTTPSClientSession.h>
#include <Poco/JSON/Parser.h>
#include <Poco/URI.h>

class Client::ClientImpl {
public:
    ClientImpl(const std::string& service_url, const std::string& token);

    Json::Value GetMe();
    std::vector<Message> GetUpdates(int64_t offset = 0, int64_t timeout = 0);
    Json::Value SendSticker(int64_t chat_id, const std::string& sticker_path);
    Json::Value SendPicture(int64_t chat_id, const std::string& picture_path,
                            int64_t reply_msg_id = 0, const std::string& caption = "");

    Json::Value SendMessage(int64_t chat_id, const std::string& msg, int64_t reply_msg_id = 0);

private:
    Json::Value MakeRequest(const std::string& request_method, const std::string& method,
                            const std::unordered_map<std::string, std::string>& params = {},
                            const std::vector<std::pair<std::string, std::string>>& headers = {});
    std::string bot_prefix_;
    std::string base_prefix_;
    std::unique_ptr<Poco::Net::HTTPClientSession> session_;
};

Client::ClientImpl::ClientImpl(const std::string& service_url, const std::string& token)
    : bot_prefix_("/bot" + token + "/"), base_prefix_(service_url) {
    Poco::URI uri(service_url);
    session_ = std::make_unique<Poco::Net::HTTPClientSession>(uri.getHost(), uri.getPort());
    const std::string& scheme(uri.getScheme());

    if (scheme == "http") {
        session_ = std::make_unique<Poco::Net::HTTPClientSession>(uri.getHost(), uri.getPort());
    } else if (scheme == "https") {
        session_ = std::unique_ptr<Poco::Net::HTTPClientSession>(
            new Poco::Net::HTTPSClientSession(uri.getHost(), uri.getPort()));
    } else {
        throw std::runtime_error("Invalid protocol");
    }
}

Json::Value Client::ClientImpl::GetMe() {
    return MakeRequest(Poco::Net::HTTPRequest::HTTP_GET, "getMe");
}

Json::Value Client::ClientImpl::SendPicture(int64_t chat_id, const std::string& picture_path,
                                            int64_t reply_msg_id, const std::string& caption) {
    std::unordered_map<std::string, std::string> params;
    params["photo"] = picture_path;
    params["chat_id"] = std::to_string(chat_id);
    if (reply_msg_id > 0) {
        params["reply_to_message_id"] = std::to_string(reply_msg_id);
    }
    if (!caption.empty()) {
        params["caption"] = caption;
    }
    return MakeRequest(Poco::Net::HTTPRequest::HTTP_POST, "sendPhoto", params,
                       {{"Content-Type", "application/json"}});
}

Json::Value Client::ClientImpl::SendSticker(int64_t chat_id, const std::string& sticker_path) {
    std::unordered_map<std::string, std::string> params;
    params["sticker"] = sticker_path;
    params["chat_id"] = std::to_string(chat_id);
    return MakeRequest(Poco::Net::HTTPRequest::HTTP_POST, "sendSticker", params,
                       {{"Content-Type", "application/json"}});
}

Json::Value Client::ClientImpl::SendMessage(int64_t chat_id, const std::string& msg,
                                            int64_t reply_msg_id) {
    std::unordered_map<std::string, std::string> params;
    params["text"] = msg;
    params["chat_id"] = std::to_string(chat_id);
    if (reply_msg_id > 0) {
        params["reply_to_message_id"] = std::to_string(reply_msg_id);
    }
    return MakeRequest(Poco::Net::HTTPRequest::HTTP_POST, "sendMessage", params,
                       {{"Content-Type", "application/json"}});
}

std::vector<Message> Client::ClientImpl::GetUpdates(int64_t offset, int64_t timeout) {
    std::unordered_map<std::string, std::string> params;
    if (timeout != 0) {
        params["timeout"] = std::to_string(timeout);
    }
    if (offset != 0) {
        params["offset"] = std::to_string(offset);
    }
    auto raw_updates = MakeRequest(Poco::Net::HTTPRequest::HTTP_GET, "getUpdates", params);

    std::vector<Message> updates;

    for (auto update : raw_updates) {
        updates.emplace_back(update);
    }
    return updates;
}

Json::Value Client::ClientImpl::MakeRequest(
    const std::string& request_method, const std::string& method,
    const std::unordered_map<std::string, std::string>& params,
    const std::vector<std::pair<std::string, std::string>>& headers) {
    Poco::URI uri(bot_prefix_ + method);

    Poco::Net::HTTPRequest request(request_method, uri.toString());

    for (const auto& header : headers) {
        request.set(header.first, header.second);
    }

    if (request_method == Poco::Net::HTTPRequest::HTTP_GET) {
        Poco::URI::QueryParameters parameters;
        for (const auto& param : params) {
            parameters.emplace_back(param);
        }
        uri.setQueryParameters(parameters);
        request.setURI(uri.toString());
        session_->sendRequest(request);
    } else {
        Poco::JSON::Object obj;
        for (const auto& param : params) {
            obj.set(param.first, param.second);
        }

        std::stringstream ss;

        obj.stringify(ss);
        request.setContentLength(ss.str().size());

        std::ostream& out = session_->sendRequest(request);
        obj.stringify(out);
    }

    Poco::Net::HTTPResponse response;
    std::istream& response_body = session_->receiveResponse(response);
    Poco::Net::HTTPResponse::HTTPStatus status = response.getStatus();

    if (status != Poco::Net::HTTPResponse::HTTPStatus::HTTP_OK) {
        throw TelegramApiError(static_cast<int>(status), response.getReason());
    }
    Json::Value value;
    std::string errors;

    if (!Json::parseFromStream(Json::CharReaderBuilder(), response_body, &value, &errors)) {
        throw JsonConvertationError("Could not get json from response...");
    }
    if (!value["ok"]) {
        throw TelegramApiError(static_cast<int>(value["error_code"].asInt()),
                               value["description"].asString());
    }
    return value["result"];
}

Client::Client(const std::string& base_url, const std::string& token) {
    impl_ = std::make_shared<ClientImpl>(base_url, token);
}

void Client::GetMe() {
    impl_->GetMe();
}

void Client::SendMessage(int64_t chat_id, const std::string& text, int64_t reply_to_message_id) {
    impl_->SendMessage(chat_id, text, reply_to_message_id);
}

void Client::SendPhoto(int64_t chat_id, const std::string& file, int64_t reply_msg_id,
                       const std::string& caption) {
    impl_->SendPicture(chat_id, file, reply_msg_id, caption);
}

void Client::SendSticker(int64_t chat_id, const std::string& sticker_path) {
    impl_->SendSticker(chat_id, sticker_path);
}

std::vector<Message> Client::GetUpdates(int64_t offset, int64_t timeout) {
    return impl_->GetUpdates(offset, timeout);
}

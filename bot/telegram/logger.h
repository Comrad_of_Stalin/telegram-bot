#pragma once

#include <string>
#include <memory>

class Logger {
public:
    Logger() : log_file_("bot_log.txt") {
    }
    Logger(std::string log_file);
    void Info(const std::string& msg);
    void Debug(const std::string& msg);
    void Error(const std::string& msg);
    void Warn(const std::string& msg);

private:
    class LoggerImpl;
    std::shared_ptr<LoggerImpl> impl_;
    std::string log_file_;
};

Logger& GetLogger();

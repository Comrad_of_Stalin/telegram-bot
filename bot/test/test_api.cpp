#include <catch.hpp>
#include <telegram/bot.h>
#include <telegram/fake.h>
#include <telegram/fake_data.h>

TEST_CASE("Single getMe scenario") {
    telegram::FakeServer fake("Single getMe");
    fake.Start();
    std::string host = fake.GetUrl(), token = "123";
    Client client(host, token);

    try {
        client.GetMe();
        REQUIRE(true);
    } catch (std::exception& ex) {
        std::cout << ex.what() << "\n";
        REQUIRE(false);
    }
}

TEST_CASE("getMe error handling") {
    telegram::FakeServer fakeServer("getMe error handling");
    fakeServer.Start();
    std::string host = fakeServer.GetUrl(), token = "123";
    Client client(host, token);

    try {
        client.GetMe();
    } catch (TelegramApiError& ex) {
        REQUIRE(ex.http_code_ == 500);
        REQUIRE(true);
    }
    try {
        client.GetMe();
    } catch (TelegramApiError& ex) {
        REQUIRE(ex.http_code_ == 401);
        REQUIRE(true);
    }
}

TEST_CASE("Single getUpdates and send messages") {
    telegram::FakeServer fakeServer("Single getUpdates and send messages");
    fakeServer.Start();
    std::string host = fakeServer.GetUrl(), token = "123";
    Client client(host, token);

    try {
        auto updates = client.GetUpdates();
        auto first_chat_id = updates[0].GetChatId();
        auto second_chat_id = updates[1].GetChatId();

        auto second_message_id = updates[1].GetMessageId();

        client.SendMessage(first_chat_id, "Hi!");
        client.SendMessage(second_chat_id, "Reply", second_message_id);
        client.SendMessage(second_chat_id, "Reply", second_message_id);
        REQUIRE(true);
    } catch (TelegramApiError& ex) {
        std::cout << ex.what() << " " << ex.http_code_ << " " << ex.msg_ << "\n";
        REQUIRE(false);
    }
}

TEST_CASE("Handle getUpdates offset") {
    telegram::FakeServer fakeServer("Handle getUpdates offset");
    fakeServer.Start();
    std::string host = fakeServer.GetUrl(), token = "123";
    Client client(host, token);

    try {
        auto updates = client.GetUpdates(0, 5);
        REQUIRE(updates.size() == 2u);

        int64_t update_id = std::max(updates[0].GetUpdateId(), updates[1].GetUpdateId()) + 1;

        auto new_updates = client.GetUpdates(update_id, 5);

        REQUIRE(new_updates.size() == 0u);

        // server sends 1 message
        REQUIRE(true);
    } catch (TelegramApiError& ex) {
        std::cout << ex.what() << " " << ex.http_code_ << " " << ex.msg_ << "\n";
        REQUIRE(false);
    }
}

TEST_CASE("Single picture sending") {
    telegram::FakeServer fake("Single picture sending");
    fake.Start();
    std::string host = fake.GetUrl(), token = "123";
    Client client(host, token);

    int64_t chat_id;
    int64_t message_id;
    std::string picture_address = "http://greenbelarus.info/files/comedy.jpg";

    try {
        auto updates = client.GetUpdates();
        REQUIRE(updates.size() == 1u);

        chat_id = updates[0].GetChatId();
        message_id = updates[0].GetMessageId();
        REQUIRE(chat_id == 104519755);
    } catch (...) {
        REQUIRE(false);
    }

    try {
        client.SendPhoto(chat_id, picture_address);
        REQUIRE(true);
    } catch (...) {
        REQUIRE(false);
    }

    try {
        client.SendPhoto(chat_id, picture_address, message_id);
        REQUIRE(true);
    } catch (...) {
        REQUIRE(false);
    }

    try {
        client.SendPhoto(chat_id, picture_address, 0, "test_caption");
        REQUIRE(true);
    } catch (...) {
        REQUIRE(false);
    }
}

TEST_CASE("Single sticker sending") {
    telegram::FakeServer fake("Single sticker sending");
    fake.Start();
    std::string host = fake.GetUrl(), token = "123";
    Client client(host, token);

    int64_t chat_id;
    std::string sticker_id = "CAADAgADPQADzhenCzhROkfYXf-NAg";

    try {
        auto updates = client.GetUpdates();
        REQUIRE(updates.size() == 1u);

        chat_id = updates[0].GetChatId();
        REQUIRE(chat_id == 104519755);
    } catch (...) {
        REQUIRE(false);
    }

    try {
        client.SendSticker(chat_id, sticker_id);
        REQUIRE(true);
    } catch (...) {
        REQUIRE(false);
    }
}
